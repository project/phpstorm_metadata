<?php

/**
 * @file
 * Contains the code to generate the custom drush commands.
 */

use Psr\Log\LogLevel;

/**
 * Implements hook_drush_command().
 */
function phpstorm_metadata_drush_command() {
  $items = [];
  $items['phpstorm-metadata-generate'] = [
    'description' => 'By running this command you will generate a PHPStorm metadata file inside Drupal root.',
    'drupal dependencies' => ['phpstorm_metadata'],
    'aliases' => ['phpmg', 'pstorm-meta'],
  ];
  return $items;
}

/**
 * Call back function drush_phpstorm_metadata_generate().
 */
function drush_phpstorm_metadata_generate() {
  $logger = \Drupal::logger('phpstorm_metadata');
  try {
    $logger->log(dt('Generating PHPStorm Metadata file'), LogLevel::OK);
    /** @var Drupal\phpstorm_metadata\Service\MetadataGenerator $metadata_service */
    $metadata_service = \Drupal::service('phpstorm_metadata.generator');
    $file = $metadata_service->generate();
    file_put_contents(DRUPAL_ROOT . '/.phpstorm.meta.php', $file);
    $logger->log(dt('Done!'), LogLevel::OK);
  }
  catch (\Exception $e) {
    $logger->log(dt('An error occurred during file generation: @error', ['@error' => $e->getMessage()]), LogLevel::ERROR);
  }
}
