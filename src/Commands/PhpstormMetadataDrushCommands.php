<?php

namespace Drupal\phpstorm_metadata\Commands;

use Drupal\phpstorm_metadata\Service\MetadataGenerator;
use Drush\Commands\DrushCommands;

/**
 * Drush Commands.
 */
class PhpstormMetadataDrushCommands extends DrushCommands {

  /**
   * PHPStorm metadata generator.
   *
   * @var \Drupal\phpstorm_metadata\Service\MetadataGenerator
   */
  private $metadataGenerator;

  /**
   * PhpstormMetadataDrushCommands constructor.
   *
   * @param \Drupal\phpstorm_metadata\Service\MetadataGenerator $metadataGenerator
   *   PHPStorm metadata generator.
   */
  public function __construct(MetadataGenerator $metadataGenerator) {
    parent::__construct();
    $this->metadataGenerator = $metadataGenerator;
  }

  /**
   * Generate PHPStorm Metadata.
   *
   * @command phpstorm-metadata:generate
   * @aliases phpmg pstorm-meta
   *
   * @usage drush phpstorm-metadata:generate
   *   Generate PHPStorm Metadata.
   */
  public function generateMetadata() {
    try {
      $this->logger()->notice('Generating PHPStorm Metadata file');
      $file = $this->metadataGenerator->generate();
      file_put_contents(DRUPAL_ROOT . '/.phpstorm.meta.php', $file);
      $this->logger()->success('Done!');
    }
    catch (\Exception $e) {
      $this->logger()
        ->error("An error occurred during file generation: @error", ['@error' => $e->getMessage()]);
    }
  }

}
