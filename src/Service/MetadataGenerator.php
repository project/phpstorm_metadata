<?php

namespace Drupal\phpstorm_metadata\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Metadata generator.
 */
class MetadataGenerator {

  /**
   * The service container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * MetadataGenerator constructor.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(ContainerInterface $container, EntityTypeManagerInterface $entityTypeManager) {
    $this->container = $container;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Generate PHPStorm metadata file contents.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function generate() {
    $serviceMap = $this->getServiceMap();
    $storageMap = $this->getStorageMap();
    return $this->generateFileContents($serviceMap, $storageMap);
  }

  /**
   * Generate PHPStorm metadata file contents from provided services/storages.
   *
   * @param array $serviceMap
   *   Service map.
   * @param array $storageMap
   *   Storage map.
   *
   * @return string
   *   PHPStorm metadata file contents.
   */
  private function generateFileContents(array $serviceMap, array $storageMap) {
    ob_start();
    include __DIR__ . '/../../file_template/.phpstorm.meta.TEMPLATE.php';
    return ob_get_clean();
  }

  /**
   * Get a map of services class names keyed by service name.
   *
   * @return array
   *   Map of services class names keyed by service name.
   */
  private function getServiceMap() {
    $serviceMap = [];
    foreach ($this->container->getServiceIds() as $serviceId) {
      $service = $this->container->get($serviceId);
      // Skip non-existing service classes and private services.
      if (is_object($service) && strpos($serviceId, 'private__') !== 0) {
        $serviceMap[$serviceId] = '\\' . get_class($service) . '::class';
      }
    }
    return $serviceMap;
  }

  /**
   * Get a map of storage class names keyed by entity name.
   *
   * @return array
   *   Map of storage class names keyed by entity name.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getStorageMap() {
    $storageMap = [];
    foreach ($this->entityTypeManager->getDefinitions() as $type => $definition) {
      $class = $this->entityTypeManager->getStorage($type);
      $storageMap[$type] = '\\' . get_class($class) . '::class';
    }
    return $storageMap;
  }

}
