<?php

namespace Drupal\Tests\phpstorm_metadata\Unit;

use Drupal\Core\DependencyInjection\Container;
use Drupal\Core\DrupalKernel;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\State\State;
use Drupal\node\NodeStorageInterface;
use Drupal\phpstorm_metadata\Service\MetadataGenerator;
use Drupal\Tests\UnitTestCase;

/**
 * Tests PHPStorm metadata generator.
 *
 * @group phpstorm_metadata
 */
class MetadataGeneratorTest extends UnitTestCase {

  /**
   * Tests metadata file contents generation.
   */
  public function testGenerate() {
    $kernelMock = $this->createMock(DrupalKernel::class);
    $kernelMockClass = get_class($kernelMock);
    $stateMock = $this->createMock(State::class);
    $stateMockClass = get_class($stateMock);

    $container = $this->createMock(Container::class);
    $container->method('getServiceIds')
      ->willReturn(['kernel', 'state']);
    $container->method('get')
      ->willReturn($kernelMock, $stateMock);

    $nodeStorageMock = $this->createMock(NodeStorageInterface::class);
    $nodeStorageClass = get_class($nodeStorageMock);

    $entityTypeManager = $this->createMock(EntityTypeManager::class);
    $entityTypeManager->method('getDefinitions')
      ->willReturn(['node' => 'node']);
    $entityTypeManager->method('getStorage')->willReturn($nodeStorageMock);

    $metadataGenerator = new MetadataGenerator($container, $entityTypeManager);
    $fileContents = $metadataGenerator->generate();
    $expected = "
<?php
namespace PHPSTORM_META {

  override(\Drupal::service(0),
  map([    'kernel' =>  \\$kernelMockClass::class,
      'state' =>  \\$stateMockClass::class,
  ]));

  override(\Symfony\Component\DependencyInjection\ContainerInterface::get(0),
  map([    'kernel' =>  \\$kernelMockClass::class,
      'state' =>  \\$stateMockClass::class,
  ]));

  override(\Drupal::entityTypeManager()->getStorage(''),
  map([    'node' =>  \\$nodeStorageClass::class,
  ]));

}
";
    $this->assertEquals($expected, $fileContents);
  }

}
