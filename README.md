INTRODUCTION
------------
This module allows to export 
[PHPStorm Advanced Metadata](
https://www.jetbrains.com/help/phpstorm/ide-advanced-metadata.html) 
in order to get autocomplete functionality for next calls:
 * Drupal::service()
 * ContainerInterface::get()
 * EntityTypeManager::getStorage()
 
REQUIREMENTS
------------
This module does not require any extra contrib modules.

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/documentation/install/modules-themes/modules-8
for further information.

CONFIGURATION
-------------
No configuration needed. 
After enabling the module a new drush command will be avalaible:

Drush 9
`drush phpstorm-metadata:generate`

Drush 8
`drush phpstorm-metadata-generate`


By running this command you will generate a PHPStorm metadata file inside 
Drupal root.
